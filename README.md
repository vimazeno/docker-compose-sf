# docker-compose to run symfony app

* start app

    ```
    docker-compose up -d
    ```

* browse http://127.0.0.1:8082/

* stop app

    ```
    docker-compose down
    ```